'use strict'

import nn from 'node-notifier'
import path from 'path'

export default (title, message, state = false) => {
  let icon
  if (state) {
    icon = path.join(__dirname, '../assets/s.png')
  } else {
    icon = path.join(__dirname, '../assets/f.png')
  }
  nn.notify({
    title: title,
    message: message,
    contentImage: icon,
    sound: true
  })
}
