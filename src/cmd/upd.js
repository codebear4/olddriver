'use strict'

import notify from '../notify'

import moment from 'moment'
import request from 'request'
import util from 'util'

export default () => {
  var start = moment()

  if (typeof process.env.UPDATE_URL == 'undefined') {
    notify('Error', 'Please run init command first.')
    process.exit(0)
  }

  new Promise((resolve, reject) => {
    request.post(process.env.UPDATE_URL, (err, resp, body) => {
      if (err == null) {
        resolve(body)
      } else {
        reject(err)
      }
    })

  }).then((body) => {
    let now = moment()
    let result = util.format('Elapsed time: %s', moment.utc(now.diff(start)).format('HH:mm:ss'))
    console.log(body)
    console.log(result)
    notify('Done', result, true)
  }).catch((err) => {
    console.error(err.message)
    notify('Error', err.message)
  })
}
