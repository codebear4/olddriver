'use strict'

import cp from 'child_process'

export default (author) => {
  new Promise((resolve, reject) => {
    cp.exec('git log --since=1.week --oneline --author=' + author, (err, stdout, stderr) => {
      if (err !== null) {
        reject(err)
      } else {
        resolve(stdout)
      }
    })
  }).then((out) => {
    console.log(out)
  }).catch((err) => {
    console.error(err.message)
  })
}
