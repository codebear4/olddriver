'use strict'

import fs from 'fs'
import path from 'path'

import notify from '../notify'

export default () => {
  new Promise((resolve, reject) => {
    fs.createReadStream(path.join(__dirname, '/../../env.dev'))
      .pipe(fs.createWriteStream(path.join(process.env.HOME, '/.env.dev')))
    resolve()
  }).then(() => {
    notify('Tip', 'File .env.dev has been created in you HOME.', true)
  }).catch((err) => {
    console.error(err.message)
    notify('Error', err.message)
  })
}
