'use strict'

import envc from 'envc'
import fs from 'fs'

export default (options) => {
  new Promise((resolve, reject) => {
      fs.accessSync(options.path + options.name, fs.F_OK)
      resolve()
    }).then(envc(options))
    .catch((err) => {
      console.error(err.message)
      process.exit(0)
    })
}
