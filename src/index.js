#!/usr/bin/env node

'use strict'

import env from './env'
import init from './cmd/init'
import upd from './cmd/upd'
import lastweek from './cmd/lastweek'
import {
  options
}
from './options'

import path from 'path'
import cmd from 'commander'

env(options)

cmd.version('0.0.1')

cmd.command('init')
  .alias('i')
  .description('Initial the config file in your HOME.')
  .action((command, options) => {
    init()
  })

cmd.command('upd')
  .alias('u')
  .description('Do update action.')
  .action((command, options) => {
    upd()
  })

cmd.command('lastweek [author]')
  .alias('lw')
  .description('Show jobs done in last week')
  .action((author = '', options) => {
    lastweek(author)
  })

cmd.parse(process.argv)

if (!cmd.args.length) {
  cmd.help()
}
